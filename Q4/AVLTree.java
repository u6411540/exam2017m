
public abstract class AVLTree  {
     abstract int bf();
     abstract int h();
     abstract AVLTree insert(int v);
 	 abstract boolean isin(int v);
 	 abstract String show();
}
